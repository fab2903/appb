# Tarea Corta Seguridad

This project is a short assesment for the course SOA4ID of Instituto Tecnológico de Costa Rica for the II Semester 2018. This AppB with AppA will be used to illustarte a security break, previous Android versions had.

## General Instructions

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

```
For better results and experience, this app was designed for Android 4.1.2, API 16.
```

### Prerequisites

For a better experience, it is recommended to have the following:

```
Android Studio
Computer capable of emulating an Android Phone running on Android 4.1.2.
```

### Installing

The following instructions will guide you to install this project on your local machine and work on it using Android Studio. 
Copy the Gitlab repository URL:

```
https://gitlab.com/fab2903/appb.git
```

Then, open Android Studio, and go to 

```
File-> New-> Project from Version Control-> Git
```

On the new screen, paste the Git Repository URL and select a local folder where the project will be saved, and finally give it a name.  
Example:

```
https://gitlab.com/fab2903/appb.git
C:\Users\me\Desktop
AppB
```

Once the project is finally downloaded and opened, you need to Buid it. 
You can press F9, press the "hammer" icon located on the top mid-right or go to:

```
 Build-> Make Project
```

Finally, to install the app in your emulated Android phone, you will need to press Shift+F10, press the "play" icon  located on the top mid-right or go to:

```
 Run-> Run 'app'
```
### Testing

To test the ApppB press the Mail Icon button on the bottom right of the page. 
This will generate a call into the call log without having the explicit permissions.
This AppB will use the permission exposed by AppA. You can see the new entry on the App A, or on the phone default call log. 



## Built With

* [Android Studio](https://developer.android.com/studio/) - The IDE used

## Authors

* **Fabian Solano** - [Fab2903](https://gitlab.com/fab2903) 


## Acknowledgments

* Indragni Soft Solutions
* Android Studio Android developer guides
* Raul Madrigal - Ataque por escalación de privilegios por uso transitivo en Android